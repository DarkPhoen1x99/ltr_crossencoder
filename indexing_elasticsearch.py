import pandas as pd
import requests
import json
from utils import corpus, preprocess_khoan, preprocess_article_title, coccoctokenize
from elasticsearch import Elasticsearch, helpers


class ZaloSearch:
    def __init__(self):
        self.es = Elasticsearch(hosts="localhost", port="9200")
        self.index = "zalo"
        self.url = "http://localhost:9200"

    def index_new_sample(self, doc, count):
        requests.put(
            data=json.dumps(doc),
            url=f"{self.url}/{self.index}/_doc/{count}",
            headers={"Content-Type": "application/json"},
        )

    def indexing(self):
        count = 0
        for _, law in enumerate(corpus):
            law_id = law["law_id"]
            articles = law["articles"]
            docs = []
            for article in articles:
                doc = {
                    "law_id": law_id,
                    "article_id": article["article_id"],
                    "text": coccoctokenize(preprocess_article_title(article["title"])),
                    "type": "title",
                }
                docs.append(doc)
                khoans = article["text"].split("\n")
                for khoan in khoans:
                    doc = {
                        "law_id": law_id,
                        "article_id": article["article_id"],
                        "text": coccoctokenize(preprocess_khoan(khoan)),
                        "type": "text",
                    }
                    docs.append(doc)

            print(_, len(docs))
            for doc in docs:
                count += 1
                self.index_new_sample(doc, count)


if __name__ == "__main__":
    es = ZaloSearch()
    es.indexing()
