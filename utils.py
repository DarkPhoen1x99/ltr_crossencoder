import pandas as pd
import numpy as np
import re
import json
import os
from CocCocTokenizer import PyTokenizer
import logging
import random

# import torch
from model.modeling_phobert import CrossEncoderPhoBERT
from transformers import (
    AutoTokenizer,
    RobertaConfig,
)

Tokenizer = PyTokenizer(load_nontone_data=True)
legal_corpus_file = "./zac2021-ltr-data/legal_corpus.json"
train_file = "./zac2021-ltr-data/train_question_answer.json"
test_file = "./zac2021-ltr-data/public_test_question.json"
test_file_private = "./zac2021-ltr-private_test/private_test_question.json"


MODEL_CLASSES = {
    "phobert": (RobertaConfig, CrossEncoderPhoBERT, AutoTokenizer),
}

MODEL_PATH_MAP = {
    "phobert": "vinai/phobert-base",
}


def load_tokenizer(args):
    return MODEL_CLASSES[args.model_type][2].from_pretrained(
        args.model_name_or_path, do_lower_case=args.do_lower_case
    )


def init_logger():
    logging.basicConfig(
        format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
        datefmt="%m/%d/%Y %H:%M:%S",
        level=logging.INFO,
    )


def set_seed(args):
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if not args.no_cuda and torch.cuda.is_available():
        torch.cuda.manual_seed_all(args.seed)


def f2(predict, label):
    tuso = len([x for x in predict if x in label])
    if len(predict) == 0:
        precision = 0
    else:
        precision = tuso / len(predict)
    recall = tuso / len(label)
    f2_mauso = 4 * precision + recall
    if f2_mauso == 0:
        return 0
    return 5 * precision * recall / (4 * precision + recall)


with open(legal_corpus_file, "r") as f:
    corpus = json.load(f)
with open(train_file, "r") as f:
    data = json.load(f)
with open(test_file, "r") as f:
    data_test = json.load(f)
with open(test_file_private, "r") as f:
    data_test_private = json.load(f)
df = pd.DataFrame(data["items"])
df_test = pd.DataFrame(data_test["items"])
df_test_private = pd.DataFrame(data_test_private["items"])

re_thuchientheo = re.compile(
    r"((((được\s)?thực hiện theo qu[iy] định tại\s|hướng dẫn tại\s|theo qu[iy] định tại\s|(được\s)?thực hiện theo\s|theo qu[iy] định tại\s|theo nội dung qu[yi] định tại\s|quy[iy] định tại|theo\s)(các\s)?)?|tại\s(các\s)?)(khoản(\ssố)?\s(\d+\,\s)*\d+|điều(\ssố)?\s(\d+\,\s)*\d+|điểm\s(([a-z]|đ)\,\s)*([a-z]|đ)\b|chương(\ssố)?\s(\d+\,\s)*\d+)((\s|\,\s|\s\,\s|\svà\s)(khoản(\ssố)?\s(\d+\,\s)*\d+|điều(\ssố)?\s(\d+\,\s)*\d+|điểm\s(([a-z]|đ)\,\s)*([a-z]|đ)\b|chương(\ssố)?\s(\d+\,\s)*\d+))*(\s(điều này|thông tư này|nghị quyết này|quyết định này|nghị định này|văn bản này|quyết định này))?"
)
re_thongtuso = re.compile(
    r"(thông tư liên tịch|thông tư|nghị quyết|quyết định|nghị định|văn bản)\s(số\s)?(([a-z0-9]|đ|\-)+\/([a-z0-9]|đ|\-|\/)*)"
)
re_ngay = re.compile(r"ngày\s\d+\/\d+\/\d+\b|ngày\s\d+tháng\d+năm\d+")
re_thang_nam = re.compile(r"tháng\s\d+\/\d+|tháng\s\d+|năm\s\d+")
re_chuong = re.compile(
    r"chương\s(iii|ii|iv|ix|viii|vii|vi|xi|xii|xiii|xiv|xix|xviii|xvii|xvi|xv|xx|v|x|i|xxiii|xxii|xxi|xxiv|xxviii|xxvii|xxvi|xxv|xxix|xxx)\b"
)


def remove_dieu_number(text):
    text = re_thuchientheo.sub(" ", text)
    text = re_thongtuso.sub(" ", text)
    text = re_ngay.sub(" ", text)
    text = re_thang_nam.sub(" ", text)
    text = re_chuong.sub(" ", text)
    return " ".join(text.split())


def remove_other_number_by_zero(text):
    for digit in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]:
        text = text.replace(digit, "0")
    return text


def remove_punct(text):
    text = text.replace(";", ",").replace(":", ".").replace("“", " ").replace("”", " ")
    text = "".join(
        [
            c
            if c.isalpha() or c.isdigit() or c in [" ", ",", "(", ")", ".", "/", "-"]
            else " "
            for c in text
        ]
    )
    text = " ".join(text.split())
    return text


def preprocess_article_title(article_title):
    article_title = article_title.lower()
    article_title = " ".join(article_title.split()[2:])  # Dieu 1.
    article_title = remove_dieu_number(article_title)
    article_title = remove_other_number_by_zero(article_title)
    article_title = remove_punct(article_title)
    return article_title


def preprocess_khoan(khoan):
    khoan = khoan.lower()
    matched = re.match(r"^\d+\.(\d+\.?)?\s", khoan)  # 1. 2.2. 2.2
    if matched is not None:
        khoan = khoan[matched.span()[1]:].strip()

    else:
        matched2 = re.match(r"^[\wđ]\)\s", khoan)
        if matched2 is not None:
            khoan = khoan[matched2.span()[1]:].strip()

    khoan = remove_dieu_number(khoan)
    khoan = remove_other_number_by_zero(khoan)
    khoan = remove_punct(khoan)
    return " ".join(khoan.split())


def coccoctokenize(text, lowercase=True):
    if lowercase:
        text = text.lower()
    tokens = Tokenizer.word_tokenize(text)
    tokens = [x for x in tokens if any(c.isalpha() for c in x)]
    return " ".join(tokens)


END_PHRASES = [
    "có đúng không",
    "đúng không",
    "được không",
    "hay không",
    "được hiểu thế nào",
    "được quy định cụ thể là gì",
    "được quy định như thế nào",
    "được quy định thế nào",
    "được quy định như nào",
    "trong trường hợp như nào",
    "trong trường hợp như thế nào",
    "trong trường hợp nào",
    "trong những trường hợp nào",
    "được hiểu như thế nào",
    "được hiểu như nào",
    "như thế nào",
    "thế nào",
    "như nào",
    "là gì",
    "là ai",
    "là bao nhiêu",
    "bao nhiêu",
    "trước bao lâu",
    "là bao lâu",
    "bao lâu",
    "bao gồm gì",
    "không",
    "bao gồm những gì",
    "vào thời điểm nào",
    "gồm những giấy tờ gì",
    "những yêu cầu nào",
]


def preprocess_question(q, remove_end_phrase=True):
    q = q.lower()
    q = remove_dieu_number(q)
    q = "".join([c if c.isalpha() or c.isdigit() or c == " " else " " for c in q])
    q = remove_punct(q)
    if remove_end_phrase:
        for phrase in END_PHRASES:
            if q.endswith(phrase):
                q = q[: -len(phrase)]
                break

    return q.strip()


STOPWORDS = [
    "bao nhiêu",
    "bị",
    "bởi",
    "các",
    "cái",
    "cần",
    "chỉ",
    "chiếc",
    "cho",
    "chứ",
    "có",
    "cứ",
    "của",
    "cũng",
    "do",
    "đã",
    "đang",
    "đây",
    "để",
    "đến",
    "đó",
    "được",
    "gì",
    "giữa",
    "khi",
    "là",
    "lên",
    "lúc",
    "mà",
    "nào",
    "này",
    "như",
    "nhưng",
    "những",
    "nơi",
    "nữa",
    "ơi",
    "ở",
    "phải",
    "qua",
    "ra",
    "rằng",
    "rất",
    "rồi",
    "sẽ",
    "so",
    "sự",
    "tại",
    "theo",
    "thì",
    "thuộc",
    "trong",
    "từ",
    "và",
    "vẫn",
    "vậy",
    "việc",
    "về",
    "vì",
    "với",
    "vừa",
    "như thế nào",
    "thế nào",
    "như nào",
    "nội dung",
]
STOPWORDS = ["_".join(word.split()) for word in STOPWORDS]


unimportant_words = [
    "điều",
    "chương",
    "sửa đổi",
    "bổ sung",
    "thi hành",
    "xử lý",
    "ban hành",
    "hướng dẫn",  #
    "phê duyệt",  #
    "nhiệm vụ",  #
    "quốc gia",  #
    "giai đoạn",
    "phát triển",  #
    "điều chỉnh",  #
    "trực thuộc",  #
    "trung ương",  #
    "dự án",  #
    "vận hành",
    "khai thác",
    "công trình",  #
    "trình tự",
    "thủ tục",
    "phương pháp",
    "xác định",
    "quản lý",  #
    "áp dụng",
    "sử dụng",
    "giải quyết",
    "chương trình",
    "kế hoạch",
    "cơ chế",
    "lĩnh vực",
    "phương thức",
    "nhà nước",
    "hình thành",  #
    "quy chế",
    "phê chuẩn",
    "kết quả",
    "hoạt động",
    # 'báo cáo',
    "biện pháp",
    "công việc",
    "tính chất",
    "lĩnh vực",
    "danh mục",
    "chế độ",  #
    "liên quan",
    "công tác",
    "phương thức",
    "quy tắc",
    "chi tiết",
    "kèm theo",
    "phù hợp",
    "địa điểm",
]
unimportant_words = ["_".join(word.split()) for word in unimportant_words]


def remove_stopwords(text):  # text of tokened
    tokens = text.split()
    tokens = [
        token
        for token in tokens
        if token not in STOPWORDS
        # and token not in unimportant_words
    ]
    return " ".join(tokens)


def gen_ngram(tokens, ngram=2):
    if len(tokens) >= ngram:
        ngram_words = []
        for i in range(ngram - 1, len(tokens)):
            ngram_words.append(" ".join(tokens[i - ngram + 1: i + 1]))
        return ngram_words
    else:
        return [" ".join(tokens)]


stupid_article_starting_patterns_after_tokenize = [
    "phạt cảnh_cáo hoặc phạt tiền từ đồng đến đồng đối với hành_vi",
    "phạt cảnh_cáo hoặc phạt tiền từ đồng đến đồng đối với một trong các hành_vi",
    "phạt cảnh_cáo hoặc phạt tiền từ đồng đến đồng đối với một trong những hành_vi sau đây",
    "phạt tiền từ đồng đến đồng đối với cá_nhân từ đồng đến đồng đối với",
    "phạt tiền từ đồng đến đồng đối với hành_vi",
    "phạt tiền từ đồng đến đồng đối với",
    "phạt tiền từ đồng đến đồng đối với một trong các hành_vi sau",
    "theo quy_định tại hiệp_định",
    "trong luật này các từ_ngữ dưới đây được hiểu như sau",
    "trong nghị_định này các từ_ngữ dưới đây được hiểu như sau",
    "trong nghị_định này các từ_ngữ sau đây được hiểu như sau",
    "trong nghị_định này các từ_ngữ được hiểu như sau",
    "trong nghị_định này những từ_ngữ dưới đây được hiểu như sau",
    "trong quyết_định này các từ_ngữ dưới đây được hiểu như sau",
    "trong thông_tư này các từ sau đây được hiểu như sau",
    "trong thông_tư này các thuật ngữ dưới đây được hiểu như sau",
    "trong thông_tư này các từ_ngữ dưới đây được hiểu như sau",
    "trong thông_tư này các từ_ngữ sau đây được hiểu như sau",
    "trong thông_tư này các từ_ngữ được hiểu như sau",
    "trong thông_tư này những từ_ngữ dưới đây được hiểu như sau",
]
stupid_van = [
    "uỷ",
    "uỵ",
    "uý",
    "uỳ",
    "oá",
    "oà",
    "oạ",
    "oẻ",
]
