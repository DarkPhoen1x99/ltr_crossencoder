import pandas as pd
import numpy as np
import re
import json
import os
import requests
from utils import (
    corpus,
    preprocess_khoan,
    preprocess_article_title,
    df,
    df_test,
    df_test_private,
    coccoctokenize,
    preprocess_question,
)


class ZaloSearch:
    def __init__(self):
        self.index = "zalo"
        self.url = "http://localhost:9200"


es = ZaloSearch()


def search_hard(q, size=100, max_len=50):
    body = {
        "from": 0,
        "size": size,  # 50
        "sort": ["_score"],
        "query": {
            "bool": {
                "must": {
                    "match": {
                        "text": {
                            "query": q,
                        }
                    }
                }
            }
        },
    }
    r = requests.get(
        data=json.dumps(body),
        url=f"{es.url}/{es.index}/_doc/_search",
        headers={"Content-Type": "application/json"},
    )
    res = r.json()
    try:

        res = res["hits"]["hits"]
        tmp = [(x["_source"]["law_id"], x["_source"]["article_id"]) for x in res]
        hard_negative_samples = []
        appeared = set()
        for law_id, article_id in tmp:
            code = f"{law_id}::::{article_id}"
            if code not in appeared:
                appeared.add(code)
                hard_negative_samples.append((law_id, article_id))
        hard_negative_samples = hard_negative_samples[:max_len]  # 10
        return hard_negative_samples
    except Exception as e:
        print(res)
        print(e)
        return []


def search_article_by_code(law_id, article_id):
    for law in corpus:
        if law["law_id"] == law_id:
            for article in law["articles"]:
                if article["article_id"] == article_id:
                    return article


def filter_articles_by_code(law_id, article_ids):
    filtered = []
    for law in corpus:
        if law["law_id"] == law_id:
            for article in law["articles"]:
                if article["article_id"] not in article_ids:
                    filtered.append(article)
            return filtered
    raise ValueError(f"not found law_id = {law_id}")


def article_to_text(article):
    khoans = article["text"].split("\n")
    text_khoans = []
    for khoan in khoans:
        text_khoans.append(coccoctokenize(preprocess_khoan(khoan)))
    text_khoans = " ".join(text_khoans)
    text_title = coccoctokenize(preprocess_article_title(article["title"]))
    text = " ".join((text_title + " " + text_khoans).split())
    return text


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--step", type=str, required=True)
    args = parser.parse_args()
    step = args.step
    if step == "train":
        data = []
        for row in df[["question", "relevant_articles", "question_id"]].values.tolist():
            doc = {
                "question_id": row[2],
                "question": row[0],
                "relevant_articles": [],
                "non_relevant_articles": [],
            }
            law_to_article = {}
            for e in row[1]:
                law_id = e["law_id"]
                article_id = e["article_id"]
                article = search_article_by_code(law_id, article_id)

                doc["relevant_articles"].append(
                    {
                        "law_id": law_id,
                        "article_id": article_id,
                        "title": article["title"],
                        "text": article["text"],
                    }
                )

            question = coccoctokenize(preprocess_question(row[0]))
            searched_hard = search_hard(question, size=50, max_len=10)
            for e in searched_hard:
                law_id = e[0]
                article_id = e[1]
                article = search_article_by_code(law_id, article_id)
                doc["non_relevant_articles"].append(
                    {
                        "law_id": law_id,
                        "article_id": article_id,
                        "title": article["title"],
                        "text": article["text"],
                    }
                )
            data.append(doc)

        with open("./data/train_data_elasticsearch.json", "w") as f:
            json.dump(data, f)
    elif step == "test":
        data = []
        for row in df_test[["question", "question_id"]].values.tolist():
            doc = {
                "question_id": row[1],
                "question": row[0],
                "articles": [],
            }

            question = coccoctokenize(preprocess_question(row[0]))
            searched_hard = search_hard(question)
            for e in searched_hard:
                law_id = e[0]
                article_id = e[1]
                article = search_article_by_code(law_id, article_id)
                doc["articles"].append(
                    {
                        "law_id": law_id,
                        "article_id": article_id,
                        "title": article["title"],
                        "text": article["text"],
                    }
                )
            data.append(doc)

        with open("./data/test_data_elasticsearch.json", "w") as f:
            json.dump(data, f)
    elif step == "test_private":
        data = []
        for row in df_test_private[["question", "question_id"]].values.tolist():
            doc = {
                "question_id": row[1],
                "question": row[0],
                "articles": [],
            }
            question = coccoctokenize(preprocess_question(row[0]))
            searched_hard = search_hard(question)
            for e in searched_hard:
                law_id = e[0]
                article_id = e[1]
                article = search_article_by_code(law_id, article_id)
                doc["articles"].append(
                    {
                        "law_id": law_id,
                        "article_id": article_id,
                        "title": article["title"],
                        "text": article["text"],
                    }
                )
            data.append(doc)

        with open("./data/test_private_data_elasticsearch.json", "w") as f:
            json.dump(data, f)
