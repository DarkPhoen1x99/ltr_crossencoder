# LTR_CrossEncoder: Legal Text Retrieval Zalo AI Challenge 2021

We propose a cross encoder model (LTR_CrossEncoder) for information retrieval, re-retrieval text relevant base on result of elasticsearch
- Model achieved 0.747 F2 score in public test (Legal Text Retrieval Zalo AI Challenge 2021)
- If using elasticsearch only, our F2 score is 0.54

## Algorithm design

Our algorithm includes two key components:
- Elasticsearch
- Cross Encoder Model

## Elasticsearch

Elasticsearch is used for filtering top-k most relevant articles based on BM25 score.
### Indexing

Instead of indexing each article as an elasticsearch's document, we use each line of an article. The answer for the query usually belongs to a sentence of an article or article title, so our indexing strategy makes sense.
Our indexing `zalo` includes 4 fields:
```
{
    'law_id':
    'article_id':
    'text':
    'type': 'title' or 'text'
}
```
Note that  instead of using elasticsearch vietnamese plugins, field `text` is tokenized by Vietnamese CocCocTokenizer before indexing.

To initialize `zalo` index, run this API (out setting) (make sure elasticsearch server is running): `PUT http://localhost:9200/zalo`
```
{
    "settings": {
        "index": {
            "number_of_shards" : 1,
            "number_of_replicas" : 1
        }
    },
    "mappings": {
        "properties": {
            "law_id" : {
                "type": "keyword"
            },
            "article_id" : {
                "type": "keyword"
            },
            "text": {
                "type": "text",
                "analyzer": "standard"
            },
            "type" : {
                "type": "keyword"
            }
        }
    }
}
```
Next, preprocess data and add them to`zalo` index:
`python indexing_elasticsearch.py`

### Getting data for the model
- Run `python run_elasticsearch.py --step {train|test|test_private}` to output `{train|test|test_private}_data_elasticsearch.json` files
- Run `python preprocessing.py --step {train|test|test_private}` to output `{train|test|test_private}_data_model.json` files

## Cross Encoder Model
<p align="center">
<img width="" alt="model" src="Model.jpg">
</p>

Our model accepts query, article text (passage) and article title as inputs and outputs a relevant score of that query and that article. Higher score, more relavant. We use pretrained `vinai/phobert-base` and `BCELoss` as loss function

### Train dataset
Non-relevant samples in dataset are obtained by `top-10` result of elasticsearch,
the training data (`train_data_model.json`) has format as follow:
```
[
    {
        "question_id": "..."
        "question": "..."
        "relevant_articles":[
            {
                "law_id": "..."
                "article_id": "..."
                "title": "..."
                "text": "..."
            },
            ...
        ]
        "non_relevant_articles":[
            {
                "law_id": "..."
                "article_id": "..."
                "title": "..."
                "text": "..."
            },
            ...
        ]
    },
    ...
]
```

### Test dataset
First we use elasticsearch to obtain k relevant candidates (`k=top-50` result of elasticsearch), then LTR_CrossEncoder classify which actual relevant article. The test data (`test_data_model.json` and `test_private_data_model.json`) has format as follow:
```
[
    {
        "question_id": "..."
        "question": "..."
        "articles":[
            {
                "law_id": "..."
                "article_id": "..."
                "title": "..."
                "text": "..."
            },
            ...
        ]
    },
    ...
]
```

### Training
Run the following bash file to train model:
```
bash run_phobert.sh
```

### Inference

We use threshold `0.9` to decide an article as relevant or not.
We also provide model checkpoints. Please download these checkpoints if you want to make inference on a new text file without training the models from scratch. Create new checkpoint folder, unzip model file and push it in checkpoint folder.
https://drive.google.com/file/d/1oT8nlDIAatx3XONN1n5eOgYTT6Lx_h_C/view?usp=sharing

Run the following bash file to infer test dataset:
```
bash run_predict.sh
```
or infer test private dataset
```
bash run_predict_private.sh
```

## Folder Structure
```
.
├── checkpoint
│   ├── config.json
│   ├── pytorch_model.bin
│   └── training_args.bin
├── data
│   ├── test_data_elasticsearch.json
│   ├── test_data_model.json
│   ├── test_private_data_elasticsearch.json
│   ├── test_private_data_model.json
│   ├── train_data_elasticsearch.json
│   └── train_data_model.json
├── data_loader.py
├── indexing_elasticsearch.py
├── main.py
├── model
│   ├── __init__.py
│   ├── modeling_phobert.py
│   └── __pycache__
│       ├── __init__.cpython-37.pyc
│       └── modeling_phobert.cpython-37.pyc
├── Model.jpg
├── predict.py
├── preprocessing.py
├── README.md
├── requirements.txt
├── run_elasticsearch.py
├── run_phobert.sh
├── run_predict_private.sh
├── run_predict.sh
├── trainer.py
├── utils.py
├── zac2021-ltr-data
│   ├── legal_corpus.json
│   ├── public_test_question.json
│   └── train_question_answer.json
└── zac2021-ltr-private_test
    └── private_test_question.json

6 directories, 31 files
```
